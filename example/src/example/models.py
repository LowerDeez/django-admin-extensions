from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.utils.translation import ugettext_lazy as _


__all__ = ('AllFields',)


class AllFields(models.Model):
    title = models.CharField(max_length=256, blank=True)
    body = models.TextField(blank=False)
    amount = models.DecimalField(max_digits=20, decimal_places=8)
    count = models.BigIntegerField()
    is_disabled = models.BooleanField(blank=True, null=True)
    identifiers_list = ArrayField(models.TextField())
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('All fields')
        verbose_name_plural = _('All fields items')
